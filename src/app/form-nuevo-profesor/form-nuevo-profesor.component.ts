import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { Profesor } from '../models/profesor.model';

@Component({
  selector: 'app-form-nuevo-profesor',
  templateUrl: './form-nuevo-profesor.component.html',
  styleUrls: ['./form-nuevo-profesor.component.css']
})
export class FormNuevoProfesorComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Profesor>;
  fg: FormGroup;
  minLong: Number = 3;
  searchResults: String[];

  constructor(fb: FormBuilder) {
    this.searchResults = [];
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ["", Validators.compose([
        Validators.required,
        //this.nameValidator,
        this.nameValidatorParams(this.minLong)
      ])],
      asignatura: ["", Validators.compose([
        Validators.required,
        this.claveValidatorParams(4)
      ])]
    });
    this.fg.valueChanges.subscribe((form: any) => {
      console.log(form);
    });
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 4),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(() => ajax('../assets/datos.json'))
      ).subscribe(ajaxResponse => {
        console.log(ajaxResponse);
        console.log(ajaxResponse.response);
        this.searchResults = ajaxResponse.response;
      })
  }

  guardar(nombre: String, asignatura: String): boolean {
    const p = new Profesor(nombre, asignatura);
    this.onItemAdded.emit(p);
    return false
  }

  nameValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return { invalidName: true }
    } else {
      return null
    }
  }

  nameValidatorParams(minLong: Number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongName: true }
      }
      return null;
    }
  }

  claveValidatorParams(minLong: Number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongClave: true }
      }
      return null;
    }
  }

}
