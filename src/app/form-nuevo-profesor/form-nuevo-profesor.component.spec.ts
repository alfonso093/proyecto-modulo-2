import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNuevoProfesorComponent } from './form-nuevo-profesor.component';

describe('FormNuevoProfesorComponent', () => {
  let component: FormNuevoProfesorComponent;
  let fixture: ComponentFixture<FormNuevoProfesorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNuevoProfesorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNuevoProfesorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
