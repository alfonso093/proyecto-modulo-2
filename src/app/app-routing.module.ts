import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaProfesoresComponent } from './lista-profesores/lista-profesores.component';
import { ProfesorDetalleComponent } from './profesor-detalle/profesor-detalle.component';

const routes: Routes = [
  { path: 'home', component: ListaProfesoresComponent },
  { path: 'detalle', component: ProfesorDetalleComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
