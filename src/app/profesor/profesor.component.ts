import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { Profesor } from '../models/profesor.model';
import { ProfesorApiClient } from '../models/profesor-api-client.model';

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.css']
})
export class ProfesorComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'col-md-4 mt-3';
  @Input() profesor: Profesor;
  @Input() idx: number;
  @Output() clicked: EventEmitter<Profesor>;

  constructor() {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir() {
    this.clicked.emit(this.profesor);
    this.profesor.btnColor = "btn-info";
    return false;
  }

}
