import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Profesor } from '../models/profesor.model'
import { ProfesorApiClient } from '../models/profesor-api-client.model';

@Component({
  selector: 'app-lista-profesores',
  templateUrl: './lista-profesores.component.html',
  styleUrls: ['./lista-profesores.component.css']
})
export class ListaProfesoresComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Profesor>;
  updates: String[];

  constructor(public profesoresApiClient: ProfesorApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.profesoresApiClient.subscribeOnChange((p: Profesor) => {
      if(p != null){
        this.updates.push("Se ha elegido a " + p.nombre);
      }
    });
  }

  ngOnInit(): void {
  }

  agregado(p: Profesor) {
    this.profesoresApiClient.add(p);
    this.onItemAdded.emit(p);
  }

  elegido(p: Profesor) {
    this.profesoresApiClient.elegir(p)
  }

}
