import { BehaviorSubject, Subject } from 'rxjs';
import { Profesor } from './profesor.model';

export class ProfesorApiClient {
    profesores: Profesor[];
    current: Subject<Profesor>;

    constructor() {
        this.profesores = [];
        this.current = new BehaviorSubject<Profesor>(null);
    }

    add(p: Profesor) {
        this.profesores.push(p);
    }

    getAll(): Profesor[] {
        return this.profesores;
    }

    getById(id: String): Profesor {
        return this.profesores.filter(d => { return d.id.toString == id; })[0];
    }

    elegir(p: Profesor) {
        this.profesores.forEach(x => (
            x.setSelected(false),
            x.btnColor = "btn-outline-info")
        );
        p.setSelected(true);
        this.current.next(p);
    }

    subscribeOnChange(fn) {
        this.current.subscribe(fn);
    }
}