export class Profesor {
    private selected: boolean;
    public descripcion: String[];
    public btnColor: String;
    id: any;
    constructor(public nombre: String, public asignatura: String) {
        this.descripcion = ['matutino', 'asignatura', 'asesorias']
        this.btnColor = "btn-outline-info"
    }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s: boolean) {
        this.selected = s;
    }
}