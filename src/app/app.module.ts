import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaProfesoresComponent } from './lista-profesores/lista-profesores.component';
import { ProfesorComponent } from './profesor/profesor.component';
import { ProfesorDetalleComponent } from './profesor-detalle/profesor-detalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormNuevoProfesorComponent } from './form-nuevo-profesor/form-nuevo-profesor.component';
import { ProfesorApiClient } from './models/profesor-api-client.model';


@NgModule({
  declarations: [
    AppComponent,
    ListaProfesoresComponent,
    ProfesorComponent,
    ProfesorDetalleComponent,
    FormNuevoProfesorComponent
  ],
  imports: [
  BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ProfesorApiClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
